# Long-Tables-on-HTML
Long Tables on HTML
<table>
     <thead>
        <tr>
	    <th>Date</th>
	    <th>Income</th>
	    <th>Expenditure</th>
	</tr>
    </thead>
    <tbody>
       <tr>
          <th>1st January</th>
	  <th>250</th>
	  <td>36</td>
	</tr>
	<tr>
	   <th> 2nd January</th>
	   <th>285</td>
	   <td>48</td>
	</tr>
     <!-- additional rows as above -->
     <tr>
       <th>31 st January</th>
       <td>129</td>
       <td>64</td>
     </tr>
    </tr>
  </tfoot>
</table>
